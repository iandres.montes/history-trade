
module.exports.parseDate = (date) => {
    let newDate = new Date(date)
    return newDate
}

module.exports.parseCreatedAt = (date) => {
    const getYear = date.getFullYear()
    const getMonth = date.getMonth() <10 ? `0${date.getMonth()}`: date.getMonth()
    const getDate = date.getDate() <10 ? `0${date.getDate()}`: date.getDate()
    const getHours = date.getHours() <10 ? `0${date.getHours()}`: date.getHours()
    const getMinutes = date.getMinutes() <10 ? `0${date.getMinutes()}`: date.getMinutes()
    const getSeconds = date.getSeconds() <10 ? `0${date.getSeconds()}`: date.getSeconds()

    return `${getYear}-${getMonth}-${getDate} ${getHours}:${getMinutes}:${getSeconds}`
}