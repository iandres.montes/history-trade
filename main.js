const fs = require('fs');
const csv = require('csv-parser');
const fastcsv = require('fast-csv');
const pd = require("node-pandas-js")
const api = require('kucoin-node-api');
const { parseDate, parseCreatedAt } = require("./helpers/parseInputDate");
const { getDatesInRange } = require("./helpers/getDatesInRange");
const {
    API_KEY,
    SECRET_KEY,
    PASS_PHRASE,
    ENV,
    PAGE_SIZE,
    START_DATE,
    END_DATE,
    CURRENCY,
    REPORT_FILENAME,
    MAXLINE_BY_FILE,
    ROOT
} = require("./helpers/configs")

const cliProgress = require('cli-progress');
const colors = require('ansi-colors');

const config = {
    apiKey: API_KEY,
    secretKey: SECRET_KEY,
    passphrase: PASS_PHRASE,
    environment: ENV
}

api.init(config)

const loader = new cliProgress.SingleBar({
    format: '{step_date} |' + colors.cyan('{bar}') + '| {percentage}% || {value}/{total} Steps ',
    barCompleteChar: '\u2588',
    barIncompleteChar: '\u2591',
    hideCursor: true
}, cliProgress.Presets.shades_grey);


const pageSize = PAGE_SIZE;
const start = parseDate(START_DATE) // MM/DD/YYYY
const end = parseDate(END_DATE)
const listDates = getDatesInRange(start, end)
const currency = CURRENCY
const currentPage = 1


const sleep = (ms) => {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

const getCurrency = async () => {
    let resultAccounts = await api.getAccounts();
    const resultFilter = resultAccounts.data.filter((el) => el.currency === currency)
    if (!resultFilter.length) return console.log('No results for currency: ' + currency)
    return resultFilter[0].id
}

function ChunkGenerator(items, size) {  

    chunks = []

    //array concat
    items = [].concat(items)

    //while loop to let us loop through array to extract the chunk.
    while (items.length) {
        chunks.push(
            items.splice(0, size)
        )
    }

    return chunks
}


const main = async () => {
    if (listDates.length === 0) return console.log('Invalid dates')

    let id = await getCurrency();
    let finalArray = [];

    console.log('Filter by: ', currency, 'with ID: ', id)
    console.log('Number of days calculated in the date range: ', listDates.length)
    loader.start(listDates.length, 0, {
        speed: "N/A",
    });

    let index = 1
    for (const dateItem of listDates) {

        await sleep(1000);
        let startAt = dateItem.getTime()
        let endAt = (dateItem.getTime() + 60 * 60 * 24 * 1000 - 1);

        // console.log(`Beginning of the period: ${dateItem} to ${new Date(endAt)}`)

        let params = {
            id: id,
            startAt: startAt,
            endAt: endAt,
            currentPage: currentPage,
            pageSize: pageSize
        }

        let resultLedger = await api.getAccountLedgers(params)

        if (resultLedger.data.totalNum > 0) {

            resultLedger.data.items.forEach((item, i) => {
                let temp = item;
                let date_step = parseDate(item.createdAt);
                temp.createdAt = date_step.toISOString()
                try {
                    let context = JSON.parse(temp.context)
                    let toSave = { ...context, ...temp }
                    delete toSave.context
                    finalArray.push(toSave);
                } catch (e) {
                    let toSave = { ...temp }
                    delete toSave.context
                    finalArray.push(toSave);
                }
            });

            if (resultLedger.data.totalPage > 1) {
                for (let pageNo = 2; pageNo <= resultLedger.data.totalPage; pageNo++) {
                    let params = {
                        id: id,
                        startAt: startAt,
                        endAt: endAt,
                        currentPage: pageNo,
                        pageSize: pageSize
                    }
                    //await utils.sleep(1000);
                    let childres = await api.getAccountLedgers(params)


                    childres.data.items.forEach((item) => {
                        let temp = item;
                        let parsedate = parseDate(item.createdAt);
                        temp.createdAt = parsedate.toISOString()
                        try {
                            let context = JSON.parse(temp.context)
                            let toSave = { ...context, ...temp }
                            delete toSave.context
                            finalArray.push(toSave);
                        } catch (e) {
                            let toSave = { ...temp }
                            delete toSave.context
                            finalArray.push(toSave);
                        }

                    });
                }
            }

        }

        let endate = new Date(endAt);
        loader.update(index, { step_date: `${dateItem.toISOString()} to ${endate.toISOString()}` })
        index += 1

    }

    loader.stop()

    const result = await ChunkGenerator(finalArray, MAXLINE_BY_FILE)

    result.map(async (item, i) =>{
        // file chunk to be written
        const writeStream = fs.createWriteStream(`${ROOT}/${REPORT_FILENAME}-${i}.csv`);
        const options = { headers: true };
        const generateCsv = fastcsv.write(item, options);
        generateCsv.pipe(writeStream);
        const jsonToCsv = new Promise(function (resolve, reject) {
            generateCsv
                .on('error', function (err) {
                    reject(err);
                })
                .on('end', function () {
                    resolve();
                });
        });
        await jsonToCsv;
    })
}

main()